import React from 'react';
import Parent from './HomePage/Components/Parent.js'
import ReactDOM from "react-dom";
import './index.css';
import '../src/HomePage/CSS/Parent.css';
import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom";
// import Confirmation from "./components/Home/Confirmation";
import SignUp from '../src/SignUp/Components/SignUp';
import Introduction from '../src/HomePage/Components/Introduction'
import SignIn from '../src/HomePage/Components/SignIn';
import Forget from '../src/Forget/Components/Forget.js';
import Feeds from '../src/NewsBox/Components/Feeds';
import Utility from '../src/NewsBox/Components/Utility';
import Resend from '../src/SignUp/Components/Resend';
import Confirmation from './SignUp/Components/Confirmation.js';
ReactDOM.render(
    <Router>
        <Parent>
            <Switch>
                <Route path="/signup" component={SignUp} />
                <Route path="/forget" component={Forget} />
                <Route path="/resendConfirmation" component={Resend} />
                <Route path="/confirmation" component={Confirmation} />
                
                <Route exact path="/" render={
                    () => {
                        return (
                            <div className='mainBody'>
                                <React.Fragment>
                                    <SignIn />
                                </React.Fragment>
                            </div>
                        )
                    }

                } />

                <Route path="/dashboard" render={
                    () => {
                        return (
                            <div className='parent1'>
                                <React.Fragment>
                                    <Feeds />
                                </React.Fragment>
                                <React.Fragment>
                                    <Utility />
                                </React.Fragment>
                            </div>
                        )
                    }
                } />

                {/* <Route path="/confirmation" component={Confirmation} /> */}
            </Switch>
        </Parent>
    </Router>,
    document.getElementById("root")
);
