import React, { Component } from 'react';
import '../CSS/Forget.css';
import axios from 'axios';

class Forget extends Component {
    state = {
        email: ""
    }
    submit = e => {
        
            e.preventDefault();
            console.log(this.state);
            axios.post("http://0.0.0.0/8080/api/account/forgot", this.state).then(function (response) {
                console.log(response.data);
            })
        
    }
    changehandler = (e) => {
        this.setState({ [e.target.name]: e.target.value })
    }
    render() {
        const { email } = this.state
        return (
            <div className="forget">
                <div className="box">
                    <form onSubmit={this.submit}>
                        <div>
                            <label>Email:</label>
                            <input type="text"
                                name="email"
                                value={email}
                                onChange={this.changehandler}
                            />
                        </div>
                        <div>
                            <input type="submit" value="Find Me" />
                        </div>
                    </form>

                </div>
            </div>
        );
    }
}

export default Forget;