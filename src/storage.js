export function getFromStorage(key) {
  console.log("key",key);

  if (!key) {
    return null;
  }
  try {
    const valueStr = localStorage.getItem(key);
  console.log("key",valueStr);
    
    if (valueStr) {
      return JSON.parse(valueStr);
    }
    return null;
  } catch (err) {
    return null;
  }
}

export function setInStorage(key, obj) {
  if (!key) {
    console.log("Error: Key is missing");
  }

  try {
    localStorage.setItem(key, JSON.stringify(obj));
  } catch (err) {
    console.log(err);
  }
}
