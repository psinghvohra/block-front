import React, { Component } from "react";
import axios from 'axios'
import { Link } from "react-router-dom/cjs/react-router-dom";
import { getFromStorage, setInStorage } from '../../storage';
import Feeds from "../../NewsBox/Components/Feeds";
import Introduction from "./Introduction";
import Utility from "../../NewsBox/Components/Utility";
import Dashboard from '../../NewsBox/Components/Dashboard';
class SignIn extends Component {
  state = {
    token: "",
    isLoading: true,
    userName: "",
    password: ""
  };
  // submit = e => {

  //   e.preventDefault();
  //   console.log(this.state);
  //   axios.post("http://0.0.0.0:8080/api/account/signin", this.state).then(function (response) {
  //     console.log(response.data);

  //   })
  // }
  submit = e => {

    e.preventDefault();
    const { userName, password } = this.state;

    this.setState({
      isLoading: true
    });

    // POST request to backend
    console.log("state:", this.state);
    fetch("http://0.0.0.0:8080/api/account/signin", {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        userName: userName,
        password: password
      })
    })
      .then(res => res.json())
      .then(json => {
        console.log("json", json);
        if (json.success) {
          setInStorage("the_main_app", { token: json.token });
          this.setState({
            //   signInError: json.message,
            isLoading: false,
            // signInEmail: "",
            // signInPassword: "",
            token: json.token
          });
        } else {
          this.setState({
            // signInError: json.message,
            isLoading: false,
            token: ""
          });
        }
      });
  }

  changehandler = (e) => {
    this.setState({ [e.target.name]: e.target.value })

  }
  componentDidMount() {
    const obj = getFromStorage("the_main_app");
    console.log("token:", this.state.token)
    if (obj && obj.token) {
      const { token } = obj;
      // Verify token
      console.log("token:", this.state.token)

      fetch("/api/account/verify?token=" + token)
        .then(res => res.json())
        .then(json => {
          if (json.success) {
            this.setState({
              token,
              isLoading: false
            });
          } else {
            this.setState({
              isLoading: false
            });
          }
        });
    } else {
      this.setState({
        isLoading: false
      });
    }
  }

  logout() {
    this.setState({
      isLoading: true
    });
    const obj = getFromStorage("the_main_app");

    if (obj && obj.token) {
      const { token } = obj;
      // Verify token
      fetch("/api/account/logout?token=" + token)
        .then(res => res.json())
        .then(json => {
          if (json.success) {
            this.setState({
              token: "",
              isLoading: false
            });
          } else {
            this.setState({
              isLoading: false
            });
          }
        });
    } else {
      this.setState({
        isLoading: false
      });
    }
  }
  render() {

    const { userName, password } = this.state
    if (!this.state.token) {
      return (
        <div className="parent1">
          <Introduction />
          <div className="signin">
            <div className='sign'>
              <form onSubmit={this.submit}>
                <div>
                  <label>Username:</label>
                  <input type="text"
                    name="userName"
                    value={userName}
                    onChange={this.changehandler}

                  />
                </div>
                <div>
                  <label>Password:</label>
                  <input type="password"
                    name="password"
                    value={password}
                    onChange={this.changehandler}
                  />
                </div>
                <div>
                  <input type="submit" value="SignIn" />
                </div>
                <div>
                  <Link to="/forget"> Forgot Username or Password?</Link>
                </div>
                <div>
                  <Link to="/signup">Create new Account</Link>
                </div>
              </form>
            </div>
          </div>
        </div>

      );
    }
    else if(this.state.token)
    return (

      <div className='parent1'>
        <Dashboard/>
      </div>

    );
  }
}

export default SignIn;

