import React, { Component } from 'react';
import NavigationBar from './NavigationBar';
const Parent = ({ children }) => (
        <>
      <NavigationBar />
  
      <main>
        {children}
      </main>
  
    </>
  );
export default Parent;
  