import React, { Component } from 'react';
import '../CSS/Feeds.css';
class Feeds extends Component {
    state = {}

    render() {
        return (
            <div className='newsfeeds'>
                {<ul>
                    <li class="dropdown">
                        <a href="" class="dropbtn">Sort</a>
                        <div class="dropdown-content">
                            <a href="">High Rating</a>
                            <a href="">Low Rating</a>
                            <a href="">Relevance</a>
                            <a href="">Time</a>
                        </div>
                    </li>
                    <li class="dropdown">
                        <a href="" class="dropbtn">Filters</a>
                        <div class="dropdown-content">
                            <a href="">India</a>
                            <a href="">USA</a>
                            <a href="">China</a>
                            <a href="">Russia</a>
                        </div>
                    </li>
                    <li class="dropdown">
                        <a href="" class="dropbtn">Genre</a>
                        <div class="dropdown-content">
                            <a href="">Entertainment</a>
                            <a href="">Sports</a>
                            <a href="">Politics</a>
                            <a href="">International</a>
                            <a href="">Science</a>
                            <a href="">Technology</a>
                        </div>
                    </li>
                </ul>

                }
                <div id='newsBlock'>

                    <div class="news">
                        <h1>news1</h1>
                        <button className="dislike">
                            <i class="fa fa-thumbs-o-down" aria-hidden="true"></i>
                        </button>

                        <button className="like">
                            <i class="fa fa-thumbs-o-up" aria-hidden="true"></i>
                        </button>

                    </div>
                    <div class="news">
                        <h1>news2</h1>
                        <button className="dislike">
                            <i class="fa fa-thumbs-o-down" aria-hidden="true"></i>
                        </button>

                        <button className="like">
                            <i class="fa fa-thumbs-o-up" aria-hidden="true"></i>
                        </button>

                    </div>
                    <div class="news">
                        <h1>news3</h1>
                        <button className="dislike">
                            <i class="fa fa-thumbs-o-down" aria-hidden="true"></i>
                        </button>

                        <button className="like">
                            <i class="fa fa-thumbs-o-up" aria-hidden="true"></i>
                        </button>

                    </div>
                    <div class="news">
                        <h1>news4</h1>
                        <button className="dislike">
                            <i class="fa fa-thumbs-o-down" aria-hidden="true"></i>
                        </button>

                        <button className="like">
                            <i class="fa fa-thumbs-o-up" aria-hidden="true"></i>
                        </button>

                    </div>
                    <div class="news">
                        <h1>news5</h1>
                        <button className="dislike">
                            <i class="fa fa-thumbs-o-down" aria-hidden="true"></i>
                        </button>

                        <button className="like">
                            <i class="fa fa-thumbs-o-up" aria-hidden="true"></i>
                        </button>

                    </div>
                    <div class="news">
                        <h1>news6</h1>
                        <button className="dislike">
                            <i class="fa fa-thumbs-o-down" aria-hidden="true"></i>
                        </button>

                        <button className="like">
                            <i class="fa fa-thumbs-o-up" aria-hidden="true"></i>
                        </button>

                    </div><div class="news">
                        <h1>news7</h1>
                        <button className="dislike">
                            <i class="fa fa-thumbs-o-down" aria-hidden="true"></i>
                        </button>

                        <button className="like">
                            <i class="fa fa-thumbs-o-up" aria-hidden="true"></i>
                        </button>

                    </div><div class="news">
                        <h1>news8</h1>
                    </div>
                    <div class="news">
                        <h1>news5</h1>
                    </div><div class="news">
                        <h1>news5</h1>
                    </div>

                </div>
            </div>
        );
    }
}

export default Feeds;