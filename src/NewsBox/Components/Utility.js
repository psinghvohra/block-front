import React, { Component } from 'react';
import '../CSS/Feeds.css'
import {logout} from '../../HomePage/Components/SignIn';
import { getFromStorage, setInStorage } from '../../storage';
import { Link } from 'react-router-dom/cjs/react-router-dom';

class Utility extends Component {
    state = {}
    
    render() {
        return (
            <div className='util'>
                <div class="card">
                    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"></link>
                    <h1>Pawan Singh Vohra</h1>
                    <p class="title">Software Developer</p>
                    <p>Amity University</p>
                    <a href="#"><i class="fa fa-dribbble"></i></a>
                    <a href="#"><i class="fa fa-twitter"></i></a>
                    <a href="#"><i class="fa fa-linkedin"></i></a>
                    <a href="#"><i class="fa fa-facebook"></i></a>
                    <button >Logout </button>
                </div>
                <div id='newsinput'>
                    <form>
                        <div>
                            <textarea cols="10" rows="5" charswidth="23" name="text_body" class="field-style" placeholder="Input News"></textarea>

                        </div>
                        <div>
                            <input type='submit'  value="Upload"></input>
                        </div>
                    </form>
                </div>
            </div>
        );
    }
}

export default Utility;