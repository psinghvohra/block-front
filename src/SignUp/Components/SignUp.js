import React, { Component } from 'react';
import "../CSS/SignUp.css";
import axios from 'axios';
import { Link } from 'react-router-dom/cjs/react-router-dom';
class SignUp extends Component {
    state = {
        firstName: "",
        lastName: "",
        email: "",
        userName: "",
        password: ""
    }
  

    submit = e => {
        
            e.preventDefault();
            console.log(this.state);
            axios.post("http://0.0.0.0:8080/api/account/signup",this.state).then(function (response) {
                console.log(response.data);
                console.log("hello");
                
            })

    
    }
    changehandler = (e) => {
        this.setState({[e.target.name]:e.target.value})
    }


    render() {
        const { firstName, lastName, email, userName, password } = this.state
        return (
            <div className='signup'>
                <div className='box'>
                    <form onSubmit={this.submit}>

                        <div>
                            <label>Name:</label>
                            <input type="text"
                                name="firstName"
                                value={firstName}
                                onChange={this.changehandler}
                            />
                        </div>
                        <div>
                            <label>Surname:</label>
                            <input type="text"
                                name="lastName"
                                value={lastName}
                                onChange={this.changehandler}

                            />
                        </div>
                        <div>
                            <label>Email:</label>
                            <input type="text"
                                name="email"
                                value={email}
                                onChange={this.changehandler}

                            />
                        </div>
                        <div>
                            <label>Username:</label>
                            <input type="text"
                                name="userName"
                                value={userName}
                                onChange={this.changehandler}
                            />
                        </div>
                        <div>
                            <label>Password:</label>
                            <input type="password"
                                name="password"
                                value={password}
                                onChange={this.changehandler}

                            />
                        </div>
                        <div>
                          <input type="submit" value="Create New Account" />
                        </div>
                    </form>
                </div>
            </div>
        );
    }
}

export default SignUp;