import React, { Component } from "react";
import { Link } from "react-router-dom";
import '../CSS/SignUp.css';
class Confirmation extends Component {
  constructor(props) {
    super(props);

    this.state = {
      message: ""
    };
  }

  componentDidMount() {
    fetch("http://0.0.0.0:8080/api/account/confirmation")
      .then(res => res.json())
      .then(json => {
        //console.log("json", json);
        if (json.success) {
          this.setState({
            message: json.message
          });
        } else {
          this.setState({
            message: json.message
          });
        }
      });
  }

  render() {
    const { message } = this.state;
    return (
      <div>
        {message ? <h1>{message}</h1> : null}
        <br />
        <button href="/signin">Login</button> 
        <br />
        <br />
        <button href="/resendConfirmation">ResendConfirmation</button>
        <br />
      </div>
    );
  }
}
export default Confirmation;
